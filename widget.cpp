#include <QTimer>
#include <QWheelEvent>
#include "widget.h"

Widget::Widget(QWidget *parent)
    : QOpenGLWidget(parent),
      camera_(0,0,3.5),
      center_(0,0,-1),
      light_(1.0,1.0,1.0)
{
}

Widget::~Widget()
{

}

void Widget::resizeGL(int w, int h)
{
    projectMatrix_.setToIdentity();
    projectMatrix_.perspective(45,float(w)/h,0.01f,100.0f);
}

void Widget::initializeGL()
{
    render_.initsize(0.8);
}

void Widget::paintGL()
{
    QOpenGLExtraFunctions *f = QOpenGLContext::currentContext()->extraFunctions();
    f->glClearColor(0.0, 0.0, 0.0, 0.0);
    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    QMatrix4x4 view;
    view.lookAt(camera_,center_,QVector3D{0,1,3.5});

    modelMatrix.rotate(30,0,1,0);

    render_.render(f,projectMatrix_,view,modelMatrix,camera_,light_);
    QTimer::singleShot(30,this,[this]{update();});
}

void Widget::wheelEvent(QWheelEvent *event)
{
    if (! event->pixelDelta().isNull()) {
        camera_.setZ(camera_.z() + event->pixelDelta().y()); //重置视点z值
    } else if (!event->angleDelta().isNull()) {
        camera_.setZ(camera_.z() + (event->angleDelta() / 120).y()); //重置视点z值
    }

    event->accept();
}
