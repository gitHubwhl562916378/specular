#ifndef SPECULARRENDER_H
#define SPECULARRENDER_H

#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLExtraFunctions>
#define PI 3.14159265f
class SpecularRender
{
public:
    SpecularRender() = default;
    void initsize(float r);
    void render(QOpenGLExtraFunctions *f, QMatrix4x4 &project, QMatrix4x4 &view, QMatrix4x4 &model, QVector3D &camera, QVector3D &lightLocation);

private:
    QOpenGLShaderProgram program_;
    QOpenGLBuffer vbo_;
    QVector<GLfloat> points_;
    float r_;
};

#endif // SPECULARRENDER_H
