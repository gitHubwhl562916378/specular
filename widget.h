#ifndef WIDGET_H
#define WIDGET_H

#include <QOpenGLWidget>
#include "specularrender.h"
class Widget : public QOpenGLWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

protected:
    void resizeGL(int w,int h) override;
    void initializeGL() override;
    void paintGL() override;
    void wheelEvent(QWheelEvent *event) override;

private:
    SpecularRender render_;
    QVector3D camera_,light_,center_;
    QMatrix4x4 projectMatrix_,modelMatrix;
};

#endif // WIDGET_H
