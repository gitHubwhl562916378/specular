#version 330
uniform mat4 uPMatrix,uVMatrix,uMMatrix;
uniform vec3 uLightLocation,uCamera;
layout (location = 0) in vec3 aPosition;
layout (location = 1) in vec3 aNormal;
smooth out vec3 vPosition;
smooth out vec3 vSpecular;

void pointLight(in vec3 normal,inout vec3 specular,in vec3 lightSpecular,in float shininess){
    vec3 normalTarget = aPosition + normal;
    vec3 newNormal = normalize((uMMatrix * vec4(normalTarget,1)).xyz - (uMMatrix * vec4(aPosition,1)).xyz);

    vec3 eye = normalize(uCamera - (uMMatrix * vec4(aPosition,1)).xyz);
    vec3 vp = normalize(uLightLocation - (uMMatrix * vec4(aPosition,1)).xyz);
    vec3 halfVector = normalize(eye + vp);
    float nDotViewHalfVecotr = dot(newNormal,halfVector);
    float powerFactor = max(0.0,pow(nDotViewHalfVecotr,shininess));

    specular = lightSpecular * powerFactor;
}

void main(void)
{
    gl_Position = uPMatrix * uVMatrix * uMMatrix * vec4(aPosition,1);
    vec3 specularTemp = vec3(0.0,0.0,0.0);
    pointLight(aNormal,specularTemp,vec3(0.7,0.7,0.7),20.0);
    vSpecular = specularTemp;
    vPosition = aPosition;
}
